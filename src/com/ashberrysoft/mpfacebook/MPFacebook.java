package com.ashberrysoft.mpfacebook;

import android.app.Application;

import com.ashberrysoft.mpfacebook.R;

import org.acra.*;
import org.acra.annotation.*;

/**
 * Класс, описывающий все приложение и необходимый для инициализации библиотеки ACRA, которая отправляет отчеты о сбоях в 
 * выполнении программы на электронный адрес разработчика
 * 
 * @author Vadim Oleynik <vadim.welldone@gmail.com>
 *
 */
@ReportsCrashes (mailTo = "vadim.welldone@gmail.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text, formKey = "dHVMZkxiZnJITzJFQUp4blFWMlNCUFE6MQ")
public class MPFacebook extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }
    
}
