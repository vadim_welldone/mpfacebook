package com.ashberrysoft.mpfacebook.activities;

import com.ashberrysoft.mpfacebook.R;

import com.facebook.*;
import com.facebook.model.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //start Facebook Login
        Session.openActiveSession(this, true, new Session.StatusCallback() {
           
            //callback when session changes state
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if(session.isOpened()) {
                    
                    //make request to the /me API
                    Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
                       
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            if(user != null) {
                                TextView name = (TextView)findViewById(R.id.name);
                                name.setText(user.getName());
                            }
                        }
                        
                    });
                    
                }
            }
            
        });
        
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

}
